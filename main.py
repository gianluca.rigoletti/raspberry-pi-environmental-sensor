from flask import Flask, render_template
from flask_socketio import SocketIO, send, emit
import random
import time
import sqlite3

app = Flask(__name__)
socketio = SocketIO(app)
db_file = 'data/environmental_parameters.db'
query_grouped_data = """SELECT * 
                FROM parameters_values
                WHERE timestamp >= datetime('now', '-1 days')
                GROUP BY strftime('%M', timestamp) / 5"""

query_last_row = """SELECT * 
                FROM parameters_values
                WHERE id = (select MAX(id) from parameters_values)"""


def get_grouped_data(curs):
    grouped_data = curs.execute(query_grouped_data).fetchall()
    return grouped_data


def get_instant_data(curs):
    row = curs.execute(query_last_row).fetchall()
    return row


@app.route('/')
def main_page():
    print('Sending home page')
    return render_template('index.html')

@socketio.on('get_grouped_data')
def send_initial_data(message):
    conn = sqlite3.connect(db_file)
    curs = conn.cursor()
    grouped_data = get_grouped_data(curs)
    emit('send_grouped_data', grouped_data)
    conn.close()


@socketio.on('get_instant_data')
def send_instant_data(message):
    conn = sqlite3.connect(db_file)
    curs = conn.cursor()
    instant_data = get_instant_data(curs)
    emit('send_instant_data', instant_data)
    conn.close()


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=5000, debug=True)
    conn.close()