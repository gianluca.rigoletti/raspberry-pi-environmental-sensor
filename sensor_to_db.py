import bme280
import smbus2
import time
import sqlite3

port = 1
address = 0x76
bus = smbus2.SMBus(port)
bme280.load_calibration_params(bus, address)

conn = sqlite3.connect('data/environmental_parameters.db')
curs = conn.cursor()


while True:
    data = bme280.sample(bus, address)
    pressure = data.pressure
    humidity = data.humidity
    temperature = data.temperature
    curs.execute("""INSERT INTO parameters_values VALUES
    (null, datetime('now'), ?, ?, ?)""", (temperature, pressure, humidity))
    print('T: {:.2f}, P: {:.2f}, H: {:.2f}'.format(temperature, pressure, humidity))
    conn.commit()
    time.sleep(2)
    
conn.close()  
    
